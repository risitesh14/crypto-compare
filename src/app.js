const express = require('express');
const logger = require('morgan');

const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const { fetchCryptoPrice } = require("./services/crypto-price");
// const { releaseConnection } = require( "./utils/db");

app.use(logger('dev'));
app.use(
    express.json({
        verify: (req, res, buf) => {
            req.rawBody = buf.toString('UTF-8');
        },
    }),
);
app.use(
    bodyParser.urlencoded({
        verify: (req, res, buf) => {
            req.rawBody = buf.toString('UTF-8');
        },
        extended: false,
    }),
);
app.use(cors());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

require('./routes').init(app);

/**
 * Set Interval for 60 seconds
 * It'll fetch the price from the cryptocompare API & update in the table
 */
const scheduler = setInterval(() => {
    console.log("Running the scheduler");
    fetchCryptoPrice()
}, 60 * 1000)

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    if (!err) return next();
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'develop' ? err : {};
    console.error(err, 'global.error.handler', 'Got error in next');
    return res.status(err.status || 500).json({ok: false});
});

const port = process.env.PORT || 8080;
const server = app.listen(port, () => console.log(`Server connected at ${port}`, 'server_start'));
process.on('SIGINT', () => {
    if (server.listening) {
        console.log('Closing http server.');
        server.close(() => {
            console.log('Clearing setInterval');
            clearInterval(scheduler);
            console.log('Http server closed.');
        });
    }
});
module.exports = server;
