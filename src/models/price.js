const mysqlConnection = require("../utils/db")

function updatePrices(rawDetails, displayDetails) {
    return new Promise((resolve, reject) => {
        mysqlConnection.query(
            "INSERT INTO crypto_prices(id, raw_details, display_details) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE raw_details= ?, display_details = ?", [1, rawDetails, displayDetails, rawDetails, displayDetails],
            (err, results) => {
            if (err) {
                reject(err);
            }
            console.log("Prices updated in table");
            resolve(true);
        });
    });
}

function fetchPrices() {
    return new Promise((resolve, reject) => {
        mysqlConnection.query(
            "SELECT * FROM crypto_prices WHERE id=?", [1],
            (err, results) => {
            if (err) {
                reject(err);
            }
            resolve(results);
        });
    });
}

module.exports =  {
    updatePrices,
    fetchPrices
}