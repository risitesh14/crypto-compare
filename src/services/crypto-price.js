const https = require('https');
const fetchSettings = require("../models/settings");
const {updatePrices} = require("../models/price");
const {RAW_FIELDS, DISPLAY_FIELDS} = require("../utils/constants");
const _ = require("lodash");

async function fetchCryptoPrice() {
    try {
        const settings = await fetchSettings();
        const fsyms = settings[0].fsyms;
        const tsyms = settings[0].tsyms;
        const result = await fetchFromAPI(fsyms, tsyms);
        const finalResult = processResult(result, fsyms, tsyms);
        updatePrices(JSON.stringify(finalResult.RAW), JSON.stringify(finalResult.DISPLAY));
    } catch (error) {
        console.error(error);
    }
}

function processResult(result, fsyms, tsyms) {
    try {
        const fsymsKeys = fsyms.split(',').map(item => item.trim());
        const tsymsKeys = tsyms.split(',').map(item => item.trim());
        const propertiesToFetch = [];
        fsymsKeys.forEach((fsymsKey) => {
            tsymsKeys.forEach((tsymsKey) => {
                RAW_FIELDS.forEach((field) => {
                    propertiesToFetch.push(`RAW.${fsymsKey}.${tsymsKey}.${field}`);
                });
                DISPLAY_FIELDS.forEach((field) => {
                    propertiesToFetch.push(`DISPLAY.${fsymsKey}.${tsymsKey}.${field}`);
                });
            });
        });
        const finalResult = _.pick(result, propertiesToFetch);
        return finalResult;
    } catch (error) {
        console.error(error);
        throw Error(error);
    }
}

function fetchFromAPI(fsyms, tsyms) {
    const options = {
        hostname: 'min-api.cryptocompare.com',
        port: 443,
        path: "/data/pricemultifull?fsyms="+fsyms+"&tsyms="+tsyms,
        method: 'GET'
    }
    return new Promise((resolve, reject) => {
        const req = https.request(options, res => {
            let data = '';
            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(JSON.parse(data));
            });
            }).on("error", (error) => {
                console.error(error);
                reject(error);
            })
            
        req.end()
    });
}

module.exports = {
    fetchCryptoPrice,
    fetchFromAPI,
    processResult
}