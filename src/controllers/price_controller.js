const { fetchFromAPI, processResult } = require("../services/crypto-price");
const { fetchPrices } = require("../models/price");
const fetchSettings = require("../models/settings");
const _ = require("lodash");

async function fetchPriceFromDB(fsyms, tsyms) {
    try {
        const settings = await fetchSettings();
        const fsymsFromDB = settings[0].fsyms.split(',');
        const tsymsFromDB = settings[0].tsyms.split(',');
        fsyms = _.intersection(fsymsFromDB, fsyms.split(",").map(item => item.trim())).join(',');
        tsyms = _.intersection(tsymsFromDB, tsyms.split(",").map(item => item.trim())).join(',');
        if (fsyms.length > 0 && tsyms.length > 0) {
            const result = await fetchPrices();
            const rawDetails = JSON.parse(result[0].raw_details);
            const displayDetails = JSON.parse(result[0].display_details);
            const jsonBody = {
                RAW: rawDetails,
                DISPLAY: displayDetails
            };
            const resultData = processResult(jsonBody, fsyms, tsyms);
            return { status: true, data: resultData};
        }
        return { status: false, data: { message: "Unable to find prices now. Please check later"}};
    } catch (error) {
        throw new Error(error);
    }
}

async function fetchPriceFromAPI(fsyms, tsyms) {
    try {
        const result = await fetchFromAPI(fsyms, tsyms);
        const resultData = processResult(result, fsyms, tsyms);
        return { status: true, data: resultData};
    } catch (error) {
        throw new Error(error);
    }
}

module.exports = {
    fetchPriceFromAPI,
    fetchPriceFromDB
}