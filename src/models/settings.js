const mysqlConnection = require("../utils/db")

function fetchSettings() {
    return new Promise((resolve, reject) => {
        mysqlConnection.query("SELECT fsyms, tsyms FROM api_settings", (err, results) => {
            if (err) {
                reject(err);
            }
            resolve(results);
        });
    });
}

module.exports = fetchSettings