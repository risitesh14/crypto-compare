module.exports = {
    RAW_FIELDS: [
        'CHANGE24HOUR',
        'CHANGEPCT24HOUR',
        'OPEN24HOUR',
        'VOLUME24HOUR',
        'VOLUME24HOURTO',
        'LOW24HOUR',
        'HIGH24HOUR',
        'PRICE',
        'LASTUPDATE',
        'SUPPLY',
        'MKTCAP'
    ],
    DISPLAY_FIELDS: [
        'CHANGE24HOUR',
        'CHANGEPCT24HOUR',
        'OPEN24HOUR',
        'VOLUME24HOUR',
        'VOLUME24HOURTO',
        'HIGH24HOUR',
        'PRICE',
        'FROMSYMBOL',
        'TOSYMBOL',
        'LASTUPDATE',
        'SUPPLY',
        'MKTCAP'
    ]
}