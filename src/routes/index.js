const { query, validationResult } = require('express-validator');
const { fetchPriceFromAPI, fetchPriceFromDB} = require("../controllers/price_controller");

exports.init = (app) => {
    app.get('/health', (req, res) => res.sendStatus(200));

    app.get('/service/price', [
        query("fsyms").notEmpty(),
        query("tsyms").notEmpty()
    ],
    async (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.array() });
        }
        try {
            const result = await fetchPriceFromAPI(req.query.fsyms, req.query.tsyms);
            if (result.status) {
                return res.status(200).json(result.data);
            }
            return res.status(400).json(result.data);
        } catch (err) {
            console.log("MinCryptoCompare API Failed... Fetching From Database");
            const result = await fetchPriceFromDB(req.query.fsyms, req.query.tsyms);
            if (result.status) {
                return res.status(200).json(result.data);
            }
            return res.status(400).json(result.data);
        }
    });
};
