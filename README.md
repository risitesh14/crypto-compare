# Crypto Compare

Directories
- config
- src
    - controllers
    - models
    - routes
    - services
    - utils
    - app.js

app.js is the main entry point to the app.

To run the app

- npm install
- npm start

While starting the app, it performs the following actions

- starts app server at 8080 port
- starts scheduler which executes at every 1 minute to fetch the price from the min-api-cryptocompare API based on the fsyms, tsyms provided in the api_settings table.

Routes provided

- GET /service/price
    - This accepts two parameters fsyms & tsyms
    - Calls the min-api.cryptocompare.com api and returns the desired result
    - In case the API fails to return any result, it checks in the DB to fetch the result based on the fsyms & tsyms provided. If not found it'll return BAD REQUEST.

Suggestion
- Instead of calling the Min-api-cryptocompare every time Cache can be used.
- Scheduler will run every-minute to pick the latest prices
- It'll push to messaging queue instead of updating DB at the same time to reduce the throughput.
- Another service will be running which will read from the message queue and will update the table.
- On Update the cache should be invalidated and new data should be updated to cache.
- API will try to fetch from cache first and if not found then fetch from DB. 
