const mysql = require('mysql');
const config = require('config');
// MySQL SETUP
const mysqlConnection = mysql.createConnection({
    host     : config.dbConfig.host,
    user     : config.dbConfig.user,
    password : config.dbConfig.password,
    database : config.dbConfig.db
});

mysqlConnection.connect((err) => {
    if (err) {
        console.error('Error while connecting to MySQL: ' + err);
        return;
    }
    console.log('MySQL connection initiated on thread: ' + mysqlConnection.threadId);
});

module.exports = mysqlConnection;